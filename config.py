"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/3/28
"""

ROBOT_SECRET = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9'
ROBOT_API = 'http://127.0.0.1:5700'
ROBOT_ACCESS_TOKEN = '9vrtOMn3tGidBtzaKhQF2OUi82v4HZkA'
ROBOT_AUTH_HEADERS = {
    'Authorization': f'Bearer {ROBOT_ACCESS_TOKEN}'
}

RESIUM_API = 'http://resium-api:8000'

BLACK_GROUPS = [979825953, 140193379]

ADMIN_GROUP = 225345829
RESIUM_USER_GROUP = 399244715

# 请求后端接口的token，适用于特定接口
BOT_TOKEN = 'w1hLAtyOV3QYq0xaWcrklXzG9KpFsnuD'
# 以用户的身份请求后端接口，适用于大部分接口
RESIUM_API_AUTH_HEADERS = {
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2NjY2NjYifQ.hcP4NzJUIigurnA0c1iXmE0H-kYaIogIz-iusKzf1jc1sL6VGm3ejnATQIPVNaB-oAWJSX1_KMKK9_KxvWCGvA'
}

DINGTALK_ACCESS_TOKEN = 'c10fd5991b46481142648156bee6dbf48981277a7c6bc803b168f14f047673cc'
DINGTALK_SECRET = 'SEC0fde189fef95beb0d23a5469cccfc74c9d0da70b104cdd256cafa2a31fb7b723'

ADMINS = [1583096683, 1583096682, 497129211, 342430384]

PATTERN_CSDN = r'^(http(s)?://download\.csdn\.net/(download|detail)/).+$'
PATTERN_WENKU = r'^(http(s)?://w(en)?k(u)?\.baidu\.com/view/).+$'
PATTERN_DOCER = r'^(http(s)?://www\.docer\.com/(webmall/)?preview/).+$'
PATTERN_ZHIWANG = r'^(http(s)?://kns(8)?\.cnki\.net/KCMS/detail/).+$'
PATTERN_QIANTU = r'^(http(s)?://www\.58pic\.com/newpic/)\d+(\.html)$'
PATTERN_PUDN = r'^http(s)?://www\.pudn\.com/Download/item/id/\d+\.html$'
PATTERN_ITEYE = r'^http(s)?://www\.iteye\.com/resource/.+-\d+$'
