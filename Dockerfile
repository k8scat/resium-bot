FROM python:3.7-alpine
LABEL maintainer="hsowan <hsowan.me@gmail.com>"
EXPOSE 5000
EXPOSE 5700

WORKDIR /data/resium-bot
ENV GO_CQHTTP_VERSION="v1.0.0-beta3"
COPY . .
RUN pip install -r requirements.txt && \
    apk add --no-cache curl && \
    curl -LO https://github.com/Mrs4s/go-cqhttp/releases/download/${GO_CQHTTP_VERSION}/go-cqhttp_linux_amd64.tar.gz && \
    tar zxvf go-cqhttp_linux_amd64.tar.gz && \
    chmod +x cqhttp.cron entrypoint.sh go-cqhttp && \
    mv go-cqhttp /usr/local/bin && \
    mv cqhttp.cron /usr/local/bin && \
    rm -f go-cqhttp_linux_amd64.tar.gz README.md LICENSE && \
    apk del curl && \
    echo "* * * * * cqhttp.cron" > /etc/crontabs/root

ENTRYPOINT ["./entrypoint.sh"]
