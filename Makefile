dev:
	python main.py

freeze:
	pip freeze > requirements.txt

build-image:
	docker build -t resium-bot:latest .