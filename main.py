"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/3/28
"""

import logging
from router import app

logging.basicConfig(format="%(asctime)s %(filename)s [line:%(lineno)d] %(levelname)s - %(message)s",
                    datefmt="%Y-%m-%d %I:%M:%S",
                    level=logging.INFO)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
