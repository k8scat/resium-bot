"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2021/04/25
"""

import hmac
import json
import logging
import re

from flask import Flask, request, Response, jsonify
import config
import requests

from utils import send_private_message, send_group_message, set_group_whole_ban

app = Flask(__name__)


@app.route('/', methods=['POST'])
def receive():
    sig = hmac.new(config.ROBOT_SECRET.encode(),
                   request.get_data(), 'sha1').hexdigest()
    received_sig = request.headers['X-Signature'][len('sha1='):]
    if sig != received_sig:
        return Response(status=204)
    data = request.get_json()
    post_type = data.get('post_type', None)  # 消息类型
    logging.info(f'post_type: {post_type}')
    if post_type == 'meta_event':
        pass
    elif post_type == 'message':
        logging.info(f'data: {data}')
        message_type = data['message_type']
        if message_type == 'private':  # 私聊消息
            # user_id = data['user_id']
            pass
        elif message_type == 'group':
            user_id = data.get('user_id', None)
            group_id = data.get('group_id', None)
            if group_id == config.ADMIN_GROUP:
                msg_content = data.get('message', None)
                if re.match(r'^qc$', msg_content, flags=re.IGNORECASE):  # 查看CSDN账号
                    payload = {
                        'token': config.BOT_TOKEN
                    }
                    with requests.post(f'{config.RESIUM_API}/bot/list_csdn_accounts/',
                                       data=payload) as r:
                        if r.status_code == requests.codes.OK:
                            send_group_message(group_id, r.json()['msg'])
                        else:
                            send_group_message(user_id, '接口请求失败！')

                elif re.match(r'^q \d{6}$', msg_content, flags=re.IGNORECASE):  # 查看用户信息
                    payload = {
                        'uid': msg_content.split(' ')[1],
                        'token': config.BOT_TOKEN
                    }
                    with requests.post(f'{config.RESIUM_API}/bot/get_user/',
                                       data=payload) as r:
                        if r.status_code == requests.codes.OK:
                            data = r.json()
                            send_group_message(group_id,
                                               json.dumps(data['user']) if data['code'] == 200 else data['msg'])
                        else:
                            send_group_message(group_id, '接口请求失败！')

                elif re.match(r'^\d{6}$', msg_content):  # 激活该账号的下载功能
                    payload = {
                        'uid': msg_content,
                        'token': config.BOT_TOKEN
                    }
                    with requests.post(f'{config.RESIUM_API}/bot/set_user_can_download/',
                                       data=payload) as r:
                        if r.status_code == requests.codes.OK:
                            send_group_message(group_id, r.json()['msg'])
                        else:
                            send_group_message(group_id, '接口请求失败！')

                # 激活该账号的下载功能
                elif re.match(r'^tb \d{6}$', msg_content, flags=re.IGNORECASE):
                    payload = {
                        'uid': msg_content.split(' ')[1],
                        'token': config.BOT_TOKEN
                    }
                    with requests.post(f'{config.RESIUM_API}/bot/activate_taobao_user/',
                                       data=payload) as r:
                        if r.status_code == requests.codes.OK:
                            send_group_message(group_id, r.json()['msg'])
                        else:
                            send_group_message(group_id, '接口请求失败！')

                elif re.match(r'^n?b$', msg_content, flags=re.IGNORECASE):  # 禁言
                    if msg_content == 'b':
                        res = set_group_whole_ban(config.RESIUM_USER_GROUP)
                    else:
                        res = set_group_whole_ban(
                            config.RESIUM_USER_GROUP, enable=False)
                    if res:
                        send_group_message(group_id, '操作成功')
                    else:
                        send_group_message(group_id, '操作失败')

                elif re.match(config.PATTERN_CSDN, msg_content) or \
                        re.match(config.PATTERN_WENKU, msg_content) or \
                        re.match(config.PATTERN_DOCER, msg_content) or \
                        re.match(config.PATTERN_ZHIWANG, msg_content) or \
                        re.match(config.PATTERN_QIANTU, msg_content) or \
                        re.match(config.PATTERN_PUDN, msg_content) or \
                        re.match(config.PATTERN_ITEYE, msg_content):

                    send_group_message(group_id, '正在下载')
                    payload = {
                        'url': msg_content,
                        't': 'url'
                    }
                    with requests.post(f'{config.RESIUM_API}/download/',
                                       data=payload,
                                       headers=config.RESIUM_API_AUTH_HEADERS) as r:
                        if r.status_code == requests.codes.OK:
                            data = r.json()
                            if data['code'] == 200:
                                send_group_message(group_id, data['url'])
                            else:
                                send_group_message(group_id, data['msg'])
                        else:
                            send_group_message(group_id, '后端接口请求失败')

                elif re.match(r'^help$', msg_content, flags=re.IGNORECASE):
                    content = '1. 下载资源: url\n' \
                        '2. 查看账号: q ID\n' \
                        '3. 授权账号: ID\n' \
                        '4. (取消)禁言: (n)b\n' \
                        '5. 查看CSDN账号: qc\n' \
                        '6. 淘宝用户授权: tb ID'

                    send_group_message(group_id, content)

        else:
            pass

    return Response(status=204)


@app.route('/send_group_msg', methods=['POST'])
def group_msg():
    data = request.get_json()
    token = data.get('token', None)
    group_id = data.get('group_id', None)
    msg = data.get('msg', None)
    at_member = data.get('at_member', None)
    if token != config.BOT_TOKEN:
        return jsonify(dict(code=requests.codes.forbidden, msg='无权访问'))

    if send_group_message(group_id, msg, at_member):
        return jsonify(dict(code=requests.codes.ok, msg='QQ群消息发送成功'))
    else:
        return jsonify(dict(code=requests.codes.server_error, msg='QQ群消息发送失败'))


@app.route('/send_private_msg', methods=['POST'])
def private_msg():
    data = request.get_json()
    token = data.get('token', None)
    user_id = data.get('user_id', None)
    msg = data.get('msg', None)
    if token != config.BOT_TOKEN:
        return jsonify(dict(code=requests.codes.forbidden, msg='无权访问'))

    if send_private_message(user_id, msg):
        return jsonify(dict(code=requests.codes.ok, msg='QQ私聊消息发送成功'))
    else:
        return jsonify(dict(code=requests.codes.server_error, msg='QQ私聊消息发送失败'))
