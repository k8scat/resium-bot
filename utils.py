"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/3/29
"""

import base64
import hashlib
import hmac
import json
import logging
import time
from urllib import parse

import requests

import config


def get_short_url(url):
    """
    生成短链接

    https://zhuanlan.zhihu.com/p/43348582

    :param url:
    :return
    """

    headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Token': '599899227931471a4e48c50e92495880'
    }
    body = {
        'Url': url,
        'TermOfValidity': '1-year'
    }
    with requests.post('https://dwz.cn/admin/v2/create', data=json.dumps(body), headers=headers) as r:
        print(r.text)
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['Code'] == 0:
                return data['ShortUrl']
            else:
                return None


def send_private_message(user_id, message):
    """发送私聊消息"""

    payload = {
        'user_id': user_id,
        'message': message
    }
    with requests.post(config.ROBOT_API + '/send_private_msg', data=payload, headers=config.ROBOT_AUTH_HEADERS) as r:
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['status'] == 'ok':
                return True
            else:
                logging.error(data)
                return False


def send_group_message(group_id, message, at_member=None):
    """发送群消息"""

    payload = {
        'group_id': group_id,
        'message': message if at_member is None else f'[CQ:at,qq={at_member}] {message}'
    }
    with requests.post(config.ROBOT_API + '/send_group_msg', data=payload, headers=config.ROBOT_AUTH_HEADERS) as r:
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['status'] == 'ok':
                return True
            else:
                logging.error(data)
                return False


def set_friend_add_request(flag, approve, remark=''):
    """处理加好友请求"""

    payload = {
        'flag': flag,
        'approve': approve,
        'remark': remark
    }
    with requests.post(config.ROBOT_API + '/set_friend_add_request', data=payload, headers=config.ROBOT_AUTH_HEADERS) as r:
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['status'] == 'ok':
                return True
            else:
                logging.error(data)
                return False


def get_group_member_list(group_id):
    """获取群成员列表"""

    payload = {
        'group_id': group_id
    }
    with requests.post(config.ROBOT_API + '/get_group_member_list', data=payload, headers=config.ROBOT_AUTH_HEADERS) as r:
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['status'] == 'ok':
                return [user['user_id'] for user in data['data']]
            else:
                logging.error(data)
                return False


def check_user_in_group(user_id):
    """判断用户是否在指定的群里"""

    for group_id in config.ENABLED_GROUPS:
        return user_id in get_group_member_list(group_id)

    return False


def get_friend_list():
    """判断用户是否QQ好友"""

    with requests.post(config.ROBOT_API + '/get_friend_list', headers=config.ROBOT_AUTH_HEADERS) as r:
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['status'] == 'ok':
                return [user['user_id'] for user in data['data']]
            else:
                logging.error(data)
                return False


def ding(message, at_mobiles=None, is_at_all=False):
    """
    使用钉钉Webhook Robot监控线上系统

    :param message:
    :param at_mobiles:
    :param is_at_all:
    :return:
    """

    timestamp = round(time.time() * 1000)
    secret_enc = config.DINGTALK_SECRET.encode('utf-8')
    string_to_sign = f'{timestamp}\n{config.DINGTALK_SECRET}'
    string_to_sign_enc = string_to_sign.encode('utf-8')
    hmac_code = hmac.new(secret_enc, string_to_sign_enc,
                         digestmod=hashlib.sha256).digest()
    sign = parse.quote_plus(base64.b64encode(hmac_code))

    if at_mobiles is None:
        at_mobiles = []
    headers = {
        'Content-Type': 'application/json'
    }

    data = {
        'msgtype': 'text',
        'text': {
            'content': message
        },
        'at': {
            'atMobiles': at_mobiles,
            'isAtAll': is_at_all
        }
    }
    dingtalk_api = f'https://oapi.dingtalk.com/robot/send?access_token={config.DINGTALK_ACCESS_TOKEN}&timestamp={timestamp}&sign={sign}'
    requests.post(dingtalk_api, data=json.dumps(data), headers=headers)


def set_group_add_request(flag, sub_type, approve=True, reason=None):
    payload = {
        'flag': flag,
        'sub_type': sub_type,
        'approve': approve,
        'reason': reason
    }
    with requests.post(config.ROBOT_API + '/set_group_add_request', data=payload,
                       headers=config.ROBOT_AUTH_HEADERS) as r:
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['status'] == 'ok':
                return True
            else:
                logging.error(data)
                return False


def set_group_whole_ban(group_id, enable=True):
    """
    群组全员禁言

    :param group_id: 群号
    :param enable: 是否禁言
    :return:
    """

    payload = {
        'group_id': group_id,
        'enable': enable
    }
    with requests.post(config.ROBOT_API + '/set_group_whole_ban', data=payload, headers=config.ROBOT_AUTH_HEADERS) as r:
        if r.status_code == requests.codes.OK:
            data = r.json()
            if data['status'] == 'ok':
                return True
            else:
                logging.error(data)
                return False
